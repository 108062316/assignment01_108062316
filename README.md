# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| 橡皮擦和畫筆的cursor顯示優化                                 | 1~5%     | Y         |


---

### How to use 

    拉動brushsize下的slider可以改變筆刷粗細
    點擊色框後可以在調色盤上選擇顏色
    可以選擇字體和字型大小。
    點擊選擇檔案後可以選擇圖片上傳
    點擊savePic後可以下載當前圖片
    點擊reset會重置畫布
    點擊drawline後在canvas上按住左鍵移動可以畫線
    點擊drawCir後在canvas上按住左鍵移動後放開可以畫圓
    點擊drawRec後在canvas上按住左鍵移動後放開可以畫長方形
    點擊drawTri後在canvas上按住左鍵移動後放開可以畫等腰三角形
    點擊eraser後在canvas上按住左鍵移動可以擦掉東西
    點擊drawText後點擊canvas會出現輸入框，輸入完成按enter，捨棄輸入按esc，輸入過程中滑鼠點擊畫布其他地方無反應，更換筆刷視為捨棄輸入
    點擊redo可以redo
    點擊undo可以undo
    
### Function description

    拉動brushsize下的slider可以改變筆刷粗細
    點擊色框後可以在調色盤上選擇顏色
    可以選擇字體和字型大小。
    點擊選擇檔案後可以選擇圖片上傳
    點擊savePic後可以下載當前圖片
    點擊reset會重置畫布
    點擊drawline後在canvas上按住左鍵移動可以畫線
    點擊drawCir後在canvas上按住左鍵移動後放開可以畫圓
    點擊drawRec後在canvas上按住左鍵移動後放開可以畫長方形
    點擊drawTri後在canvas上按住左鍵移動後放開可以畫等腰三角形
    點擊eraser後在canvas上按住左鍵移動可以擦掉東西
    點擊drawText後點擊canvas會出現輸入框，輸入完成按enter，捨棄輸入按esc，輸入過程中滑鼠點擊畫布其他地方無反應，更換筆刷視為捨棄輸入
    點擊redo可以redo
    點擊undo可以undo
    不同的筆刷對應不同的cursor
    drawline、drawCir、drawRec、drawTri對應到的cursor會隨著選擇的顏色不同而改變顯示顏色
    drawline、eraser對應到的cursor會隨著筆刷粗細而調整大小

### Gitlab page link

   https://108062316.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
