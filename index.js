var mouse = {
    x : 0,
    y : 0,
}
var IsDrawLine = false;
var IsDrawRec = false;
var IsDrawTri = false;
var IsDrawCir = false;
var IsDrawText = false;
var IsEraser = false;
var IsDrawLineCalled = false;
var imageArr = [];
var nowImage = 0;
var nowMax = 0;
var drawRecTime = 0;
var drawLineTime = 0;
var drawCirTime = 0;
var drawTriTime = 0;
var drawEraseTime = 0;
const once = {
    once : true
  };


function savePic(){
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    cvs.style.cursor = "default";
    console.log("in save");
    var myLink=document.getElementById("download");
    myLink.download="image.png";
    myLink.href=cvs.toDataURL("image/png");
    myLink.click();
}

function uploadPic(input) {
    checkIsEmpty();
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    cvs.style.cursor = "default";
    console.log("lalala");
    ctx.globalCompositeOperation="source-over";
    var myPic=input.files[0];
    var src=URL.createObjectURL(myPic);
    var myImg = new Image();
    myImg.src = src;
    myImg.onload=function(){
        ctx.drawImage(this,0,0,cvs.width,cvs.height);
        updateArr();
    };
}

function resetCanvas(){
    IsDrawLine = false;
    IsDrawRec = false;
    IsDrawTri = false;
    IsDrawCir = false;
    IsEraser = false;
    IsDrawText = false;
    textStartx = -1;
    textStarty = -1;
    resetCanvas2();
    resetCanvas3();
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    cvs.style.cursor = "default";
    //console.log("reset");

    document.getElementById("myFile").value = "";
    ctx.clearRect(0,0,cvs.width,cvs.height);
    nowImage = 0;
    while(imageArr.length>0) imageArr.pop();
    checkIsEmpty();
    console.log("now there are:",imageArr.length);
    console.log("we're at",nowImage);
}

function resetCanvas2(){
    var cvs,ctx;
    cvs = document.getElementById("canvas2");
    ctx = cvs.getContext('2d');
    //console.log("reset");
    ctx.clearRect(0,0,cvs.width,cvs.height);
}

function resetCanvas3(){
    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    //console.log("reset");
    ctx.clearRect(0,0,cvs.width,cvs.height);
}

function redo(){
    console.log("redo")
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    if(nowImage>0){
        nowImage = nowImage-1;
        ctx.putImageData(imageArr[nowImage], 0, 0);
    }
}

function undo(){
    console.log("undo");
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    if(nowImage < nowMax){
        console.log("true in")
        nowImage = nowImage+1;
        ctx.putImageData(imageArr[nowImage], 0, 0);
    }
}

function eraser(){
    checkIsEmpty();
    resetCanvas2();
    IsDrawLine = false;
    IsDrawRec = false;
    IsDrawTri = false;
    IsDrawCir = false;
    IsDrawText = false;
    IsEraser = true;
    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    console.log("in eraser");
    if(IsEraser){
        cvs.style.cursor = "none";
    }
    let isErase = false;
    cvs.addEventListener('mousedown',e => {
        if(IsEraser)
        {
            isErase = true;
            drawEraseTime = 1;
            mouse.x = e.offsetX;
            mouse.y = e.offsetY;
            console.log("erase clickdown");
        }
    })

    cvs.addEventListener('mouseup',e => {
        if(IsEraser){
            isErase = false;
            mouse.x = e.offsetX;
            mouse.y = e.offsetY;
            if(drawEraseTime == 1)
            {
                updateArr();
                drawEraseTime = 0;
            }
            console.log("erase clickup");
        }
    })

    cvs.addEventListener('mousemove',(e) => {
        //mouse.x = event.pageX;
        //mouse.y = event.pageY;
        drawCursor(e.offsetX,e.offsetY);
        if(IsEraser === true && isErase) {
            var x1 = mouse.x;
            var y1 = mouse.y;
            var x2 = e.offsetX;
            var y2 = e.offsetY;
            var ctx1,cvs1;
            cvs1 = document.getElementById("canvas");
            ctx1 = cvs1.getContext('2d');
            ctx1.globalCompositeOperation="destination-out";
            var radius;
            radius = document.getElementById("myRange")
            ctx1.lineWidth = radius.value;
            ctx1.lineJoin = 'round';
            ctx1.lineCap = 'round';
            ctx1.beginPath();
            ctx1.moveTo(x1, y1);
            ctx1.lineTo(x2, y2);
            ctx1.stroke();
            ctx1.closePath();

            mouse.x = x2;
            mouse.y = y2;

        }
    })

    cvs.addEventListener('mouseout',(e)=>{
        resetCanvas3();
    })

}
function drawLine() {
    checkIsEmpty();
    resetCanvas2();
    IsDrawLine = true;
    IsDrawRec = false;
    IsDrawTri = false;
    IsDrawCir = false;
    IsEraser = false;
    IsDrawText = false;

    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    //ctx.globalCompositeOperation="source-over";
    console.log("in draw line new");
    if(IsDrawLine)
    cvs.style.cursor = "none";
    let isDrawing = false;

    cvs.addEventListener('mousemove',(e) => {
        //mouse.x = event.pageX;
        //mouse.y = event.pageY;
        drawCursor(e.offsetX,e.offsetY);
        if(isDrawing === true && IsDrawLine) {
            CreateCir(mouse.x,mouse.y,e.offsetX,e.offsetY);
            mouse.x = e.offsetX;
            mouse.y = e.offsetY;
        }
    })

    
    cvs.addEventListener('mousedown',e => {
        //console.log("draw line mouse down");
        if(IsDrawLine){
            console.log("draw line mouse down");
            drawLineTime = 1;
            isDrawing = true;
            mouse.x = e.offsetX;
            mouse.y = e.offsetY;
        }
    })


    cvs.addEventListener('mouseout',(e)=>{
        if(IsDrawLine)
            resetCanvas3();
    })

    cvs.addEventListener('mouseup',e => {
        if(isDrawing == true && IsDrawLine)
        {
            isDrawing = false;
            //console.log("clickup");
            if(drawLineTime == 1)
            {
                updateArr();
                drawLineTime = 0;
            }
        }
    })
}

function drawRec(){
    checkIsEmpty();
    resetCanvas2();
    IsDrawLine = false;
    IsDrawRec = true;
    IsDrawTri = false;
    IsDrawCir = false;
    IsEraser = false;
    IsDrawText = false;

    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    //ctx.globalCompositeOperation="source-over";
    if(IsDrawRec)
        cvs.style.cursor = "none";
    //console.log("in draw rec");
    var start_x,start_y;
    var isDrawing = false;

    cvs.addEventListener('mousedown',e => {
        if(IsDrawRec){
            isDrawing = true;
            start_x = e.offsetX;
            start_y = e.offsetY;
            console.log("rec clickdown");
            drawRecTime = 1;
        }
    })

    cvs.addEventListener('mousemove',(e) => {
        drawCursor(e.offsetX,e.offsetY);
        if(isDrawing === true && IsDrawRec) {
            previewRec(start_x,start_y,e.offsetX,e.offsetY);
        }
    })

    cvs.addEventListener('mouseout',(e) => {
        resetCanvas3();
    })

    cvs.addEventListener('mouseup',e => {
        if(IsDrawRec && isDrawing){
            var real_x1,real_x2,real_y1,real_y2,radius;
            var colorButton = document.getElementById("myColor");
            var red = parseInt("0x"+colorButton.value.slice(1,3))
            var green = parseInt("0x"+colorButton.value.slice(3,5))
            var blue = parseInt("0x"+colorButton.value.slice(5,7))
            radius = document.getElementById("myRange");
            var ctx1,cvs1;
            cvs1 = document.getElementById("canvas");
            ctx1 = cvs1.getContext('2d');
            ctx1.globalCompositeOperation="source-over";
            ctx1.lineWidth = radius.value;
            ctx1.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
            ctx1.lineJoin = 'round';
            ctx1.lineCap = 'round';
            resetCanvas2();
            real_x1 = (start_x<e.offsetX)?start_x:e.offsetX;
            real_x2 = (start_x>=e.offsetX)?start_x:e.offsetX;
            real_y1 = (start_y<e.offsetY)?start_y:e.offsetY;
            real_y2 = (start_y>=e.offsetY)?start_y:e.offsetY;
            ctx1.beginPath();
            ctx1.rect(real_x1, real_y1, real_x2-real_x1, real_y2-real_y1);
            ctx1.stroke();
            console.log("rectangle update");
            if(drawRecTime==1)
            {
                updateArr();
                drawRecTime = 0;
            }
            //console.log("clickdown");
        }
        isDrawing = false;
    })
}

function drawTri()
{
    checkIsEmpty();
    resetCanvas2();
    IsDrawLine = false;
    IsDrawRec = false;
    IsDrawTri = true;
    IsDrawCir = false;
    IsEraser = false;
    IsDrawText = false;

    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    //ctx.globalCompositeOperation="source-over";
    if(IsDrawTri)
        cvs.style.cursor = "none";

    var start_x,start_y;
    var isDrawing = false;
    cvs.addEventListener('mousedown',e => {
        if(IsDrawTri){
            isDrawing = true;
            drawTriTime = 1;
            start_x = e.offsetX;
            start_y = e.offsetY;
            console.log("clickdown");
        }
    })

    cvs.addEventListener('mousemove',(e) => {
        drawCursor(e.offsetX,e.offsetY);
        if(isDrawing === true && IsDrawTri) {
            previewTri(start_x,start_y,e.offsetX,e.offsetY);
        }
    })

    cvs.addEventListener('mouseout',(e) => {
        resetCanvas3();
    })

    cvs.addEventListener('mouseup',e => {
        if(IsDrawTri&&isDrawing){
            console.log("finish drawing");
            var cvs1,ctx1,radius;
            cvs1 = document.getElementById("canvas");
            ctx1 = cvs1.getContext('2d');
            radius = document.getElementById("myRange")
            var colorButton = document.getElementById("myColor");
            var red = parseInt("0x"+colorButton.value.slice(1,3))
            var green = parseInt("0x"+colorButton.value.slice(3,5))
            var blue = parseInt("0x"+colorButton.value.slice(5,7))
            ctx1.lineWidth = radius.value;
            ctx1.lineJoin = 'round';
            ctx1.lineCap = 'round';
            ctx1.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
            ctx1.globalCompositeOperation="source-over";
            resetCanvas2();

            var x1,y1,x2,y2,x3,y3;
            x1 = start_x;
            y1 = start_y;
            x2 = e.offsetX;
            y2 = e.offsetY;
            if(x1<=x2) {
                x3 = x2-2*(x2-x1);
                y3 = y2;
            } 
            else if(x1>x2){
                x3 = x2+2*(x1-x2);
                y3 = y2;
            }
            ctx1.beginPath();
            ctx1.moveTo(x1, y1);
            ctx1.lineTo(x2, y2);
            ctx1.moveTo(x2, y2);
            ctx1.lineTo(x3, y3);
            ctx1.moveTo(x3, y3);
            ctx1.lineTo(x1, y1);
            ctx1.stroke();
            ctx1.closePath();
            if(drawTriTime == 1)
            {
                updateArr();
                drawTriTime = 0;
            }
        }
        isDrawing = false;
    })
  
}

function drawCir(){
    checkIsEmpty();
    resetCanvas2();
    IsDrawLine = false;
    IsDrawRec = false;
    IsDrawTri = false;
    IsDrawCir = true;
    IsEraser = false;
    IsDrawText = false;

    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    ctx.globalCompositeOperation="source-over";
    if(IsDrawCir)
    cvs.style.cursor = "none";

    var start_x,start_y;
    var isDrawing = false;
    cvs.addEventListener('mousedown',e => {
        if(IsDrawCir){
            isDrawing = true;
            start_x = e.offsetX;
            start_y = e.offsetY;
            drawCirTime = 1;
        }
       // console.log("clickdown");
    })

    cvs.addEventListener('mousemove',(e) => {
        drawCursor(e.offsetX,e.offsetY);
        if(isDrawing === true && IsDrawCir) {
            previewCir(start_x,start_y,e.offsetX,e.offsetY);
        }
    })

    cvs.addEventListener('mouseout',(e)=>{
        resetCanvas3();
    })
    cvs.addEventListener('mouseup',e => {
        isDrawing = false;
        if(IsDrawCir){
            var x1,x2,y1,y2;
            x1 = start_x;
            x2 = e.offsetX;
            y1 = start_y;
            y2 = e.offsetY;
            console.log(x1,x2,y1,y2)
            //new
            var ctx1,cvs1;
            cvs1 = document.getElementById("canvas");
            ctx1 = cvs1.getContext('2d');
            ctx1.globalCompositeOperation="source-over";
            var colorButton = document.getElementById("myColor");
            var red = parseInt("0x"+colorButton.value.slice(1,3))
            var green = parseInt("0x"+colorButton.value.slice(3,5))
            var blue = parseInt("0x"+colorButton.value.slice(5,7))
            console.log("r:",red,"g:",green,"b:",blue);
            radius = document.getElementById("myRange")
            ctx1.lineWidth = radius.value;
            ctx1.lineJoin = 'round';
            ctx1.lineCap = 'round';
            ctx1.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
            resetCanvas2();

            var real_x = (x1+x2)/2;
            var real_y = (y1+y2)/2;
            var dis = (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
            dis = Math.sqrt(dis)/2;
            ctx1.beginPath();
            ctx1.arc(real_x,real_y,dis,0,Math.PI*2,true); // Outer circle
            ctx1.stroke();
            ctx1.closePath();
            //console.log("circle update is bomb")
            if(drawCirTime == 1)
            {
                updateArr();
                drawCirTime = 0;
            }
        }
    })
}

/*function keyHandler(str){
    str.preventDefault();
    console.log(str)
}*/
var nowstr = "";
var isEnd = false;
var textStartx = 0;
var textStarty = 0;
var textFontSize = -1;
var textFontType = "";
window.addEventListener('keydown',e =>{
    console.log(e.key);
    if(e.key == "Enter")
    {
        //IsDrawText = false;
        realDrawText(textStartx,textStarty);
        resetCanvas2();
        resetCanvas3();
        if(nowstr!="" && IsDrawText)
        updateArr();

        nowstr = "";
        textStartx = -1;
        textStarty = -1;
    }
    else if(e.key == "Escape"){
        resetCanvas2();
        resetCanvas3();
        nowstr = "";
        textStartx = -1;
        textStarty = -1;
    }
    else if(e.key == "Backspace")
    {
        nowstr = nowstr.substring(0,nowstr.length-1)
        resetCanvas2();
        previewText(textStartx,textStarty)
        var cvs2,ctx2;
        cvs2 = document.getElementById("canvas2");
        ctx2 = cvs2.getContext('2d');
        ctx2.lineWidth = 1;
        ctx2.strokeStyle = 'black';
        ctx2.beginPath();
        ctx2.rect(textStartx, textStarty-textFontSize/2-3, 100, textFontSize);
        ctx2.stroke();
        console.log("after back ",nowstr);
    }
    else{
        //nowstr = nowstr+e.key;
        if(IsDrawText && textStarty>=0 &&textStartx>=0)
        {   
            nowstr = nowstr+e.key;
            previewText(textStartx,textStarty)
            var cvs2,ctx2;
            cvs2 = document.getElementById("canvas2");
            ctx2 = cvs2.getContext('2d');
            ctx2.lineWidth = 1;
            ctx2.strokeStyle = 'black';
            ctx2.beginPath();
            ctx2.rect(textStartx, textStarty-textFontSize/2-3,100,textFontSize);
            ctx2.stroke();
            console.log(nowstr);
        }
    }
})
function drawtext(){
    checkIsEmpty();
    resetCanvas2();
    IsDrawLine = false;
    IsDrawRec = false;
    IsDrawTri = false;
    IsDrawCir = false;
    IsEraser = false;
    IsDrawText = true;
    textStartx = -1
    textStarty = -1
    var fontSize,fontType;
    /*var fontSize = document.getElementById("fontSize");
      var fontType = document.getElementById("fontType");   */
    //textFontSize = fontSize.value;
    //textFontType = fontType.value;
    var cvs,ctx;
    cvs=document.getElementById("canvas3");
    ctx=cvs.getContext('2d');
    ctx.globalCompositeOperation="source-over";
    if(IsDrawText){
        cvs.style.cursor = "text";
        nowstr = ""
    }
    cvs.addEventListener('mousedown',e => {
        if(textStartx == -1 && textStarty == -1 && IsDrawText)
        {
            fontSize = document.getElementById("fontSize");
            fontType = document.getElementById("fontType");
            textFontSize = fontSize.value;
            textFontType = fontType.value;
            textStartx = e.offsetX;
            textStarty = e.offsetY;
            var cvs2,ctx2;
            cvs2 = document.getElementById("canvas2");
            ctx2 = cvs2.getContext('2d');
            ctx2.lineWidth = 1;
            ctx2.strokeStyle = 'black';
            ctx2.beginPath();
            ctx2.rect(textStartx, textStarty-textFontSize/2-3, 100, textFontSize);
            ctx2.stroke();

        }
        console.log("draw text clickdown");
    })
}

function realDrawText(x,y)
{
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    resetCanvas2();

    ctx.font = textFontSize + 'px ' + textFontType;
    ctx.textAlign = 'start';
    ctx.fillText(nowstr, x, y);
}
function previewText(x,y){
    var cvs,ctx;
    cvs = document.getElementById("canvas2");
    ctx = cvs.getContext('2d');
    var cvs2,ctx2;
    cvs2 = document.getElementById("canvas3");
    ctx2 = cvs.getContext('2d');
    ctx.globalCompositeOperation="source-over";
    console.log("real_draw")
    resetCanvas2();
    resetCanvas3();

    /*ctx2.beginPath();
    ctx2.rect(x, y, textFontSize+2, 30);
    ctx2.stroke();*/

    ctx.font = textFontSize + 'px ' + textFontType;
    //var myfont = textFontSize + 'px Arial';
    //console.log(ctx.font," ",myfont);
    ctx.textAlign = 'start';
    ctx.fillText(nowstr, x, y);
}

function CreateCir(x1,y1,x2,y2) {
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    ctx.globalCompositeOperation="source-over";
    var radius;
    radius = document.getElementById("myRange")
    var colorButton = document.getElementById("myColor");
    var red = parseInt("0x"+colorButton.value.slice(1,3))
    var green = parseInt("0x"+colorButton.value.slice(3,5))
    var blue = parseInt("0x"+colorButton.value.slice(5,7))
    ctx.lineWidth = radius.value;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.closePath();

    /*ctx.beginPath();
    ctx.arc(x1,y1,radius.value,0,Math.PI*2);
    ctx.fillStyle='black';
    ctx.fill();*/
}

function previewRec(x1,y1,x2,y2){
    var cvs,ctx,radius;
    cvs = document.getElementById("canvas2");
    ctx = cvs.getContext('2d');
    //cvs.style.zIndex = "1";
    //ctx.globalCompositeOperation="source-over";
    radius = document.getElementById("myRange")
    var colorButton = document.getElementById("myColor");
    var red = parseInt("0x"+colorButton.value.slice(1,3))
    var green = parseInt("0x"+colorButton.value.slice(3,5))
    var blue = parseInt("0x"+colorButton.value.slice(5,7))
    ctx.lineWidth = radius.value;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
    resetCanvas2()
    

    var real_x1,real_x2,real_y1,real_y2;
    real_x1 = (x1<x2)?x1:x2;
    real_x2 = (x1>=x2)?x1:x2;
    real_y1 = (y1<y2)?y1:y2;
    real_y2 = (y1>=y2)?y1:y2;
    ctx.beginPath();
    ctx.rect(real_x1, real_y1, real_x2-real_x1, real_y2-real_y1);
    ctx.stroke();
}

function previewTri(x1,y1,x2,y2){//1 start
    var cvs,ctx,radius;
    cvs = document.getElementById("canvas2");
    ctx = cvs.getContext('2d');
    radius = document.getElementById("myRange")
    var colorButton = document.getElementById("myColor");
    var red = parseInt("0x"+colorButton.value.slice(1,3))
    var green = parseInt("0x"+colorButton.value.slice(3,5))
    var blue = parseInt("0x"+colorButton.value.slice(5,7))
    ctx.lineWidth = radius.value;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
    resetCanvas2();

    var x3,y3;
    if(x1<=x2) {
        x3 = x2-2*(x2-x1);
        y3 = y2;
    } 
    else if(x1>x2){
        x3 = x2+2*(x1-x2);
        y3 = y2;
    }
    //console.log(x1,x2,x3);
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.moveTo(x2, y2);
    ctx.lineTo(x3, y3);
    ctx.moveTo(x3, y3);
    ctx.lineTo(x1, y1);
    ctx.stroke();
    ctx.closePath();
}

function previewCir(x1,y1,x2,y2){
    var cvs,ctx,radius;
    cvs = document.getElementById("canvas2");
    ctx = cvs.getContext('2d');
    radius = document.getElementById("myRange")
    var colorButton = document.getElementById("myColor");
    var red = parseInt("0x"+colorButton.value.slice(1,3))
    var green = parseInt("0x"+colorButton.value.slice(3,5))
    var blue = parseInt("0x"+colorButton.value.slice(5,7))
    ctx.lineWidth = radius.value;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
    resetCanvas2();

    var real_x = (x1+x2)/2;
    var real_y = (y1+y2)/2;
    var dis = (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
    dis = Math.sqrt(dis)/2;
    ctx.beginPath();
    ctx.arc(real_x,real_y,dis,0,Math.PI*2,true); // Outer circle
    ctx.stroke();
    ctx.closePath();
}

function drawCursor(x1,y1){
    var cvs,ctx;
    cvs = document.getElementById("canvas3");
    ctx = cvs.getContext('2d');
    radius = document.getElementById("myRange")
    var colorButton = document.getElementById("myColor");
    var red = parseInt("0x"+colorButton.value.slice(1,3))
    var green = parseInt("0x"+colorButton.value.slice(3,5))
    var blue = parseInt("0x"+colorButton.value.slice(5,7))
    ctx.strokeStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
    ctx.fillStyle = 'rgb(' + red + ',' + green + ',' + blue + ')';
    resetCanvas3();
    if(IsDrawRec){
        ctx.beginPath();
        ctx.rect(x1, y1, 10, 5);
        ctx.stroke();
        ctx.closePath();
    }
    if(IsEraser){
        ctx.beginPath();
        ctx.strokeStyle = 'black'
        ctx.rect(x1-radius.value/2, y1-radius.value/2, radius.value, radius.value);
        ctx.stroke();
        ctx.closePath();
    }
    if(IsDrawCir){
        ctx.beginPath();
        ctx.arc(x1,y1,10,0,Math.PI*2,true);
        ctx.stroke();
        ctx.closePath();
    }
    if(IsDrawLine){
        ctx.beginPath();
        //ctx.rect(x1, y1, 5, 10);
        ctx.arc(x1,y1,radius.value/2,0,Math.PI*2,true);
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
    }
    if(IsDrawTri){
        var x2 = x1+5;
        var x3 = x1-5;
        var y2,y3;
        y2=y3=y1+10;
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.moveTo(x2, y2);
        ctx.lineTo(x3, y3);
        ctx.moveTo(x3, y3);
        ctx.lineTo(x1, y1);
        ctx.stroke();
        ctx.closePath();
    }
}

function checkIsEmpty()
{
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');
    if(imageArr.length == 0){
        console.log("check")
        let emptyCanvas = ctx.getImageData(0, 0, 800, 600);
        imageArr.push(emptyCanvas);
    }
}

function updateArr()
{
    var cvs,ctx;
    cvs = document.getElementById("canvas");
    ctx = cvs.getContext('2d');

    let nowCanvas = ctx.getImageData(0, 0, 800, 600);
 
    nowImage = nowImage + 1;
    nowMax = nowImage;
    if(nowImage == imageArr.length){
        imageArr.push(nowCanvas);
    }
    else {
        imageArr[nowImage] = nowCanvas;
    }

    console.log("now there are:",imageArr.length);
    console.log("we're at",nowImage);
}
